﻿// Homework18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
#include <algorithm>
//Класс заключающий в себе имя игрока и набранные им очки.
class player
{
private:
    int points;
    std::string player_name;
public:
    player()
    {
      points = 0;
      player_name = { "" };
    }
    //метод класса для записи имени игрока
    void set_name(std::string _player_name)
    {
        player_name = _player_name;
    }
    //метод класса для записи количества очков игрока
    void set_pts(int _pts)
    {
        points = _pts;
    }
    //методы для вывода очков и имени игрока
    std::string get_name()
    {
        return player_name;
    }
    int get_pts()
    {
        return points;
    }

};
//функция сортировки
void bubblesort(int l, int r, player *pl)
{
    int sz = r - l;
    if (sz <= 1) return;
    bool b = true;
    while (b) {
        b = false;
        for (int i = l; i + 1 < r; i++) {
            if (pl[i].get_pts() < pl[i + 1].get_pts()) {
                std::swap(pl[i], pl[i + 1]);
                b = true;
            }
        }
        r--;
    }
}
int main()
{
    int x = 0;
    int pts = 0;
    std::string name;
    std::cout << "Enter amount of players" << '\n';
    std::cin >> x;
    player *players = new player[x];
        for (int plr=0;plr<x;plr++)
        {
            std::cout << "Enter  player name:";
            std::cin.get();
            getline(std::cin, name); 
            std::cout << "Enter  player points:";
            std::cin>> pts;
            players[plr].set_name(name);
            players[plr].set_pts(pts);
        }
        bubblesort(0, x, players);
        for (int plr = 0; plr < x; plr++)
        {
            std::cout << '\n' << players[plr].get_name()<<' '<<players[plr].get_pts();
            
        }
        
        delete [] players;
      
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
